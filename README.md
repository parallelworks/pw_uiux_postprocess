# Parallel Works Post-Processing UI/UX

This is a set of scripts to showcase a common Parallel Works UI/UX post-process operation.

Specifically, these post-processing scripts take output files from a common Department of Energy building energy modeling tool (located in the sim directory), and generate an HTML viewer from the files using shell and python scripting.

* * *
### Running the Post-Processor

This post-processor relies on a number of python tools that have conveniently been compiled into a docker container.

To run the post-processing scripts directly using docker, please enter the following command:
```
docker run -it -w /scratch -v $PWD:/scratch parallelworks/python-tools \
./filter.sh sim/baseline.sim sim/proposed.sim sim/baseline.sweep sim/proposed.sweep out.html
```

Or, to start the container and run the post-processing scripts interactively within the docker container, please enter the following commands
```
docker run -it -w /scratch -v $PWD:/scratch parallelworks/python-tools /bin/bash
./filter.sh sim/baseline.sim sim/proposed.sim sim/baseline.sweep sim/proposed.sweep out.html
```

* * *

### Task 1:

* Revise the postprocess.py script to create a graph of the hourly baseline and proposed simulation results using the baseline_hourly.csv and proposed_hourly.csv preprocess outputs. 
* Embed the new hourly charts into the html template.

### Task 2:

* Revise the template.html to look like Formatted_WebResults.pdf.