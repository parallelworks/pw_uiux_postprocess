#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2016 Parallel Works, Inc.
#
# USAGE: python postprocess.py sim/baseline.sweep sim/proposed.sweep out.html

import sys
import os
import pandas as pd
import numpy as np
import argparse
from jinja2 import Environment, FileSystemLoader
from weasyprint import HTML
import base64
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')

pd.set_option('precision',2) # Default to 2 decimal places for all HTML output

def gen_pie(labels,sizes,fname):
    plt.clf()
    # The slices will be ordered and plotted counter-clockwise.
    colors = ['#d7e3bf','#1072bd']
    explode = (0, 0)  # explode a slice if required
    plt.pie(sizes, explode=explode, labels=labels, colors=colors,autopct='%1.1f%%', shadow=False)
    # draw a circle at the center of pie to make it look like a donut
    centre_circle = plt.Circle((0,0),0.7,color='black', fc='white',linewidth=1.2)
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)
    #    Set aspect ratio to be equal so that pie is drawn as a circle.
    plt.axis('equal')
    # Save pie1
    plt.savefig(fname,transparent=True)

def gen_bar_diag(baseelecfile,basegasfile,propelecfile,propgasfile,fname):
  
    matplotlib.style.use('ggplot')
  
    base_month_elec = pd.read_csv(baseelecfile)
    base_month_gas = pd.read_csv(basegasfile)
    prop_month_elec = pd.read_csv(propelecfile)
    prop_month_gas = pd.read_csv(propgasfile)
    
    fig, axes = plt.subplots(nrows=2, ncols=2)
    fig.set_figwidth(12)
    #fig.set_figheight(8)
    
    colormap='jet_r'
    
    base_month_elec1=base_month_elec.ix[0:11]
    cats=base_month_elec1['Category']
    del base_month_elec1['Category']
    base_month_elec1= base_month_elec1.transpose()
    base_month_elec1.columns=cats.as_matrix()
    base_epl=base_month_elec1.plot(legend=True,ax=axes[0,0], kind='bar', stacked=True,  colormap=colormap, width=0.65);
    base_epl.legend(frameon=False,bbox_to_anchor=(0, -2.1, 2., .102), loc=3,ncol=6, mode="expand", borderaxespad=0.,fontsize=10)
    base_epl.yaxis.grid(b=True, which='major', color='black', linestyle='--', alpha=0.3)
    base_epl.xaxis.grid(b=False, which='major', color='black', linestyle='--', alpha=0)
    base_epl.tick_params( which='major', bottom='on', top='off', left='off', right='off',labelbottom='on')
    
    base_month_gas1=base_month_gas.ix[0:11]
    del base_month_gas1['Category']
    base_month_gas1= base_month_gas1.transpose()
    base_gpl=base_month_gas1.plot(legend=False,ax=axes[1,0], kind='bar', stacked=True,  colormap=colormap,width=0.65);
    base_gpl.yaxis.grid(b=True, which='major', color='black', linestyle='--', alpha=0.3)
    base_gpl.xaxis.grid(b=False, which='major', color='black', linestyle='--', alpha=0)
    base_gpl.tick_params( which='major', bottom='on', top='off', left='off', right='off',labelbottom='on')
    
    prop_month_elec1=prop_month_elec.ix[0:11]
    del prop_month_elec1['Category']
    prop_month_elec1= prop_month_elec1.transpose()
    prop_epl=prop_month_elec1.plot(legend=False,ax=axes[0,1], kind='bar', stacked=True,  colormap=colormap, width=0.65);
    prop_epl.yaxis.grid(b=True, which='major', color='black', linestyle='--', alpha=0.3)
    prop_epl.xaxis.grid(b=False, which='major', color='black', linestyle='--', alpha=0)
    prop_epl.tick_params( which='major', bottom='on', top='off', left='off', right='off',labelbottom='on')

    prop_month_gas1=prop_month_gas.ix[0:11]
    del prop_month_gas1['Category']
    prop_month_gas1= prop_month_gas1.transpose()
    prop_gpl=prop_month_gas1.plot(legend=False,ax=axes[1,1], kind='bar', stacked=True,  colormap=colormap, width=0.65);
    prop_gpl.yaxis.grid(b=True, which='major', color='black', linestyle='--', alpha=0.3)
    prop_gpl.xaxis.grid(b=False, which='major', color='black', linestyle='--', alpha=0)
    prop_gpl.tick_params( which='major', bottom='on', top='off', left='off', right='off',labelbottom='on')

    for i in range(0,2):
      for j in range(0,2):
        axes[i,j].spines['top'].set_visible(False)
        axes[i,j].spines['left'].set_visible(False)
        axes[i,j].spines['right'].set_visible(False)
        axes[i,j].spines['bottom'].set_visible(False)

    axes[0,0].set_title('Baseline: Electricity Consumption (kWh)',fontsize=12)
    axes[1,0].set_title('Baseline: Gas Consumption (MBTU)',fontsize=12)
    axes[0,1].set_title('Proposed: Electricity Consumption (kWh)',fontsize=12)
    axes[1,1].set_title('Proposed: Gas Consumption (MBTU)',fontsize=12)

    fig.tight_layout(pad=1, w_pad=2, h_pad=3.0)
    fig.subplots_adjust(bottom=0.2)
    fig.savefig(fname,transparent=True)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generate HTML report')
    parser.add_argument('infile', type=str, help="Input csv file")
    parser.add_argument('baseline', type=str, help="baseline sweep file")
    parser.add_argument('proposed', type=str, help="proposed sweep file")
    parser.add_argument('outhtml', type=str, help="output html file")
    args = parser.parse_args()

    # Read in the vars extracted from .sim output file.  Two data rows: baseline + proposed case

    v = pd.read_csv(args.infile)
    var_table = v.to_html(classes="save");
    
    baseline = pd.read_table(args.baseline, skiprows=0, header=0, sep=r"\s*", engine='python')    
    proposed = pd.read_table(args.proposed, skiprows=0, header=0, sep=r"\s*", engine='python')    

    merge = pd.merge(left=baseline,right=proposed, left_on='pname', right_on='pname', how='outer')
    merge.columns = ['Simulation Parameters', 'Baseline Variables', 'Proposed Glass Variables']

    # Derive computed values
    
    #   TABLE: Window-Wall Ratio
    
    try:
      nwwr=float(v.v10a[0])/float(v.v11a[0])
    except:
      nwwr=0
    try:
      ewwr=float(v.v10b[0])/float(v.v11b[0])
    except:
      ewwr=0
    try:
      swwr=float(v.v10c[0])/float(v.v11c[0])
    except:
      swwr=0
    try:
      wwwr=float(v.v10d[0])/float(v.v11d[0])
    except:
      wwwr=0
    
    baseglass=[merge[merge["Simulation Parameters"].isin(["North_Glass_Type"])]["Baseline Variables"].iloc[0],merge[merge["Simulation Parameters"].isin(["East_Glass_Type"])]["Baseline Variables"].iloc[0],merge[merge["Simulation Parameters"].isin(["South_Glass_Type"])]["Baseline Variables"].iloc[0],merge[merge["Simulation Parameters"].isin(["West_Glass_Type"])]["Baseline Variables"].iloc[0]]  
    propglass=[merge[merge["Simulation Parameters"].isin(["North_Glass_Type"])]["Proposed Glass Variables"].iloc[0],merge[merge["Simulation Parameters"].isin(["East_Glass_Type"])]["Proposed Glass Variables"].iloc[0],merge[merge["Simulation Parameters"].isin(["South_Glass_Type"])]["Proposed Glass Variables"].iloc[0],merge[merge["Simulation Parameters"].isin(["West_Glass_Type"])]["Proposed Glass Variables"].iloc[0]]

    data = { 'Face' : ['North', 'East', 'South', 'West'],
             'Area (sf)': [v.v11a[0],v.v11b[0],v.v11c[0],v.v11d[0]],
             'Window/Wall Ratio': [nwwr,ewwr,swwr,wwwr],
             'Baseline Glass':baseglass,
             'Proposed Glass': propglass}
    wwr = pd.DataFrame(data,columns=["Face","Area (sf)","Window/Wall Ratio","Baseline Glass","Proposed Glass"])
    wwr_table = wwr.to_html()
    floor_area = v.v16[0]

    #   TABLE: HVAC downsizing

    #      Compute savings in Supply CFM

    sc = [v.v13a[0]+v.v13b[0]+v.v13c[0], v.v13a[1]+v.v13b[1]+v.v13c[1], 0.0, 0.0]
    sc[2] = sc[0] - sc[1]
    sc[3] = (sc[2]/sc[0]) * 100.0

    #     Compute savings in Cooling Tons

    ct = [(v.v12a[0]+v.v12b[0]+v.v12c[0])/12, (v.v12a[1]+v.v12b[1]+v.v12c[1])/12, 0.0, 0.0]
    ct[2] = ct[0] - ct[1]
    ct[3] = (ct[2]/ct[0]) * 100.0

    data = { 'Glazing Scenario' : ['Baseline','Proposed Glass','Savings (absolute)','Savings (%)'],
             'Supply CFM': sc,
             'Cooling Tons': ct}

    hvd = pd.DataFrame(data, columns=['Glazing Scenario', 'Supply CFM', 'Cooling Tons'])
    hvd_table = hvd.to_html(classes="rowcolor")

    # TABLE: Electricity usage

    ck = [v.v02[0], v.v02[1], 0.0, 0.0]
    ck[2] = ck[0] - ck[1]
    ck[3] = (ck[2]/ck[0]) * 100.0

    co = [v.v07[0], v.v07[1], 0.0, 0.0]
    co[2] = co[0] - co[1]
    co[3] = (float(co[2])/float(co[0])) * 100.0

    dc = [v.v08[0], v.v08[1], 0.0, 0.0]
    dc[2] = dc[0] - dc[1]
    dc[3] = (float(dc[2])/float(dc[0])) * 100.0

    fc = [0.0, 0.0, 0.0, 0.0]

    tc = [v.v04[0], v.v04[1], 0.0, 0.0]
    tc[2] = tc[0] - tc[1]
    tc[3] = (tc[2]/tc[0]) * 100.0

    data = {'Glazing Scenario' : ['Baseline','Proposed Glass','Savings (absolute)','Savings (%)'],
            'Energy Consumption (kWh)' : ck,
            'Energy Cost' : co,
            'Demand Cost' : dc,
            'Total Cost' : tc}

    eu = pd.DataFrame(data, columns=['Glazing Scenario', 'Energy Consumption (kWh)', 'Energy Cost', 'Demand Cost', 'Total Cost'])
    eu_table = eu.to_html(classes="rowcolor")

    # TABLE: Peak Month

    kw = [v.v06[0], v.v06[1], 0.0, 0.0]
    kw[2] = kw[0] - kw[1]
    kw[3] = (kw[2]/kw[0]) * 100.0
    
    data = {'Glazing Scenario' : ['Baseline','Proposed Glass','Savings (absolute)','Savings (%)'],
            'kW' : kw}

    pm = pd.DataFrame(data, columns=['Glazing Scenario', 'kW'])
    pm_table = pm.to_html(classes="rowcolor")

    # TABLE: Natural Gas Use

    th  = [v.v03[0], v.v03[1], 0.0, 0.0]
    th[2] = th[0] - th[1]
    th[3] = (th[2]/th[0]) * 100.0
    
    cog  = [v.v05[0], v.v05[1], 0.0, 0.0]
    cog[2] = cog[0] - cog[1]
    cog[3] = (cog[2]/cog[0]) * 100.0
    
    data = {'Glazing Scenario' : ['Baseline','Proposed Glass','Savings (absolute)','Savings (%)'],
            'Therms' : th,
            'Cost' : cog}

    ng = pd.DataFrame(data, columns=['Glazing Scenario', 'Therms', 'Cost'])
    ng_table = ng.to_html(classes="rowcolor")

    # TABLE: Enery Use Intensity

    el  = [v.v14[0], v.v14[1], 0.0, 0.0]
    el[2] = el[0] - el[1]
    el[3] = (el[2]/el[0]) * 100.0
    
    ng  = [v.v15[0], v.v15[1], 0.0, 0.0]
    ng[2] = ng[0] - ng[1]
    ng[3] = (ng[2]/ng[0]) * 100.0
    
    te  = [v.v01[0], v.v01[1], 0.0, 0.0]
    te[2] = te[0] - te[1]
    te[3] = (te[2]/te[0]) * 100.0
    
    ecba  = [v.v17[0], v.v17[1], 0.0, 0.0]
    ecba[2] = ecba[0] - ecba[1]
    ecba[3] = (ecba[2]/ecba[0]) * 100.0
    
    data = {'Glazing Scenario'   : ['Baseline','Proposed Glass','Savings (absolute)','Savings (%)'],
            'Electricity (kWh/sf/year)'    : el,
            'Natural Gas (Therms/sf/year)' : ng,
            'Total Energy (kbtu/sf/year)'  : te,
            'Energy Cost/Building Area ($/sf/year)' : ecba}

    eui = pd.DataFrame(data, columns=['Glazing Scenario', 'Electricity (kWh/sf/year)', 'Natural Gas (Therms/sf/year)', 'Total Energy (kbtu/sf/year)','Energy Cost/Building Area ($/sf/year)'])
    eui_table = eui.to_html(classes="rowcolor")
    
    # Generate pie chart graphics

    labels = 'Supply CFM\nSaved', 'Supply CFM\nUsed'
    savings = hvd['Supply CFM'][3]
    sizes = [savings, 100.0 - savings]
    gen_pie(labels, sizes, 'pie1.png')

    labels = 'Cooling Tons\nSaved', 'Cooling Tons\nUsed'
    savings = hvd['Cooling Tons'][3]
    sizes = [savings, 100.0 - savings]
    gen_pie(labels, sizes, 'pie2.png')
    
    # Encode the pie charts to base64

    pie1 = base64.b64encode(open("pie1.png", "rb").read())
    pie2 = base64.b64encode(open("pie2.png", "rb").read())
    
    # remove the pies
    os.remove("pie1.png")
    os.remove("pie2.png")

    # Create the baseline and proposed variable table
    
    sim = pd.DataFrame(merge)
    sim_table = sim.to_html()
    
    # generate the monthly diagnostic bar charts
    gen_bar_diag("preprocess/baseline_monthly_elec.csv","preprocess/baseline_monthly_gas.csv","preprocess/proposed_monthly_elec.csv","preprocess/proposed_monthly_gas.csv","monthly.png")

    diagnostics = base64.b64encode(open("monthly.png", "rb").read())

    # remove the monthly
    os.remove("monthly.png")

    # Expand report template
    
    env = Environment(loader=FileSystemLoader('.'))

    template = env.get_template("template.html")
    template_vars = { "title" : "HTML Report Output",
                      "var_table" : var_table,
                      "wwr_table" : wwr_table,
                      "floor_area" : floor_area,
                      "hvd_table" : hvd_table,
                      "eu_table" : eu_table,
                      "pm_table" : pm_table,
                      "ng_table" : ng_table,
                      "eui_table" : eui_table,
                      "sim_table" : sim_table,
                      "pie1" : pie1,
                      "pie2" : pie2,
                      "diagnostics": diagnostics}

    # Render report as html and write to file

    html_out = template.render(template_vars)
    f = open(args.outhtml,'w')
    f.write(html_out)
    f.close()
