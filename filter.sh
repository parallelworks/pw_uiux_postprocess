#! /bin/sh
# -*- coding: utf-8 -*-
#
# Copyright 2016 Parallel Works, Inc.
#
# USAGE: ./filter.sh sim/baseline.sim sim/proposed.sim sim/baseline.sweep sim/proposed.sweep out.html

rm -rf preprocess
mkdir -p preprocess

filterdoe2() {

rm -rf reports
mkdir -p reports

tr -d '\015' < $1 |

tr -d '\015' | tr '\014' '\012' | # Remove CR; convert FF to NL
sed -e '/^DOE-2.2-48r/d' -e '/^-----------------------------------*------$/d' \
                       -e '/^-----------------------------------*-(CONTINUED)/d' |
awk '

BEGIN {
  fname="";
  need["^BEPS"] = 1
  need["^BEPU"] = 1
  need["^ES-D"] = 1
  need["^ES-E.*Elec"] = 1
  need["^LV-D"] = 1
  need["^LS-C.*DESIGN DAY"] = 1
  need["^SV-A.*BL1Sys1.*\\(G\\)"] = 1
  need["^SV-A.*BL1Sys1.*\\(T\\)"] = 1
  need["^SV-A.*BL1Sys1.*\\(M\\)"] = 1
  need["^Hourly Report HVAC"] = 1
  need["^Hourly Glare Report"] = 1
  need["PS-E Energy End-Use Summary for all Electric Meters"] = 1
  need["PS-E Energy End-Use Summary for all Fuel Meters"] = 1
}

/OLDBEGIN/ {
  fname="";
  # need["^SV-A.*\(VAVS\)"] = 1
  need["^SV-A"] = 1
  need["^ES-E Summary of Utility-Rate Electric Rate"] = 1
  need["^ES-E Summary of Utility-Rate Gas Rate"] = 1
  need["^PS-E Energy End-Use Summary for all Electric Meters"] = 1
  need["^PS-E Energy End-Use Summary for all Fuel Meters"] = 1
  need["^BEPS Building Energy Performance"] = 1
  need["^SS-D Building HVAC Load Summary"] = 1
  need["^SS-E Building HVAC Load Hours"] = 1
  need["^Hourly Report HVAC"] = 1
}

function needreport(rn)
{
  for (reportneeded in need) {
    if(match(rn,reportneeded)) return(1)
  }
  return(0)
}

/^REPORT|^HOURLY REPORT/ {
    rn=$0
    gsub(/^HOURLY REPORT- /,"",rn);  # Remove HOURLY REPORT- prefix
    gsub(/^REPORT- /,"",rn);         # Remove REPORT- prefix
    gsub(/WEATHER FILE.*/, "",rn);   # Remove WEATHER FILE suffix
    gsub(/:/," ",rn);                # Change : to space - not valid filename char
    gsub(/\//," ",rn);               # Change / to space - dont want extra dirnames
    gsub(/  */," ",rn);              # Remove extra spaces
    gsub(/ *$/,"",rn);               # Remove trailing spaces
    printf "rn=|%s|\n", rn
    if (fname != "") close(fname);
    if (needreport(rn))
      fname = "reports/" rn
    else
      fname = ""
}

fname != "" {
  if ( $0 ~ /^REPORT/ ) { } else print >> fname;
}
'

} # END OF filterdoe2

getvars() {

# Usage: getvars.sh [ -h | -H ] runid runtype
#                     -h        Add header line to output
#                     -H        Emit only header line, and exit
#                     Default   No header line

header()
{
echo runid,runtype,v01,v02,v03,v04,v05,v06,v07,v08,v10a,v10b,v10c,v10d,v10e,v11a,v11b,v11c,v11d,v11e,v12a,v12b,v12c,v13a,v13b,v13c,v14,v15,v16,v17
}

if [ _$1 = _-h ]; then
  header; shift
elif [ _$1 = _-H ]; then
  header; exit
fi

runid=${1:-run001}
runtype=${2:-baseline}

eval $(
cat reports/BEPS* | awk '
  /TOTAL SITE ENERGY/ { printf "v01=%s\n", $9}
' 
cat reports/BEPU* | awk '
  /^ *KWH/ { printf "v02=%s\n", $14}
  /^ *THERM/ { printf "v03=%s\n", $14}
  /^ *TOTAL ELECTRICITY/ { printf "v14=%s\n", $9}
  /^ *TOTAL NATURAL-GAS/ { printf "v15=%s\n", $9}
'
cat reports/ES-D* | awk '
  /ELECTRICITY/ { gsub(/^.*ELECTRICITY/,"",$0); printf "v04=%s\n", $4}
  /NATURAL-GAS/ { gsub(/^.*NATURAL-GAS/,"",$0); printf "v05=%s\n", $4}
 /ENERGY COST\/GROSS/ { gsub(/^.*ENERGY COST/,"",$0); printf "v17=%s\n",$4}
'
cat reports/ES-E* | awk '
  /^ *TOTAL/ { printf "v06=%s\n", $4
               printf "v07=%s\n", $5
               printf "v08=%s\n", $6 }
'
cat reports/LV-D* | awk '
  /^ *NORTH /     { printf "v10a=%s\n", $5; printf "v11a=%s\n", $7 }
  /^ *EAST /      { printf "v10b=%s\n", $5; printf "v11b=%s\n", $7 }
  /^ *SOUTH /     { printf "v10c=%s\n", $5; printf "v11c=%s\n", $7 }
  /^ *WEST /      { printf "v10d=%s\n", $5; printf "v11d=%s\n", $7 }
  /^ *ALL WALLS/  { printf "v10e=%s\n", $5; printf "v11e=%s\n", $7 }
'
cat reports/LS-C* | awk '
  /^ *FLOOR  AREA/ { printf "v16=%s\n", $3 }
'

cat 'reports/SV-A System Design Parameters for BL1Sys1 (VAVS) (G)' | awk '
  /^VAVS /    { printf "v12a=%s\n", $6 }
  /^  SUPPLY/ { printf "v13a=%s\n", $2 }
'
cat 'reports/SV-A System Design Parameters for BL1Sys1 (VAVS) (T)' | awk '
  /^VAVS /    { printf "v12b=%s\n", $6 }
  /^  SUPPLY/ { printf "v13b=%s\n", $2 }
'
cat 'reports/SV-A System Design Parameters for BL1Sys1 (VAVS) (M)' | awk '
  /^VAVS /    { printf "v12c=%s\n", $6 }
  /^  SUPPLY/ { printf "v13c=%s\n", $2 }
'
) 

echo $runid, $runtype, $v01, $v02, $v03, $v04, $v05, $v06, $v07, $v08, $v10a, $v10b, $v10c, $v10d, $v10e, $v11a, $v11b, $v11c, $v11d, $v11e, $v12a, $v12b, $v12c, $v13a, $v13b, $v13c, $v14, $v15, $v16, $v17

} # End of function getvars()

getdetails() {

name=$1  

awk < 'reports/PS-E Energy End-Use Summary for all Electric Meters' '
BEGIN {

# Energy categories

c[0] = "Area Lights"
c[1] = "Task Lights"
c[2] = "Misc Equip"
c[3] = "Space Heating"
c[4] = "Space Cooling"
c[5] = "Heat Reject"
c[6] = "Pumps & Aux"
c[7] = "Vent Fans"
c[8] = "Refrig Display"
c[9] = "Ht Pump Supplem"
c[10] = "Domest Hot Wtr"
c[11] = "Ext Usage"
c[12] = "Total"

nc = length(c)

# Month labels

m[0] = "JAN"
m[1] = "FEB"
m[2] = "MAR"
m[3] = "APR"
m[4] = "MAY"
m[5] = "JUN"
m[6] = "JUL"
m[7] = "AUG"
m[8] = "SEP"
m[9] = "OCT"
m[10] = "NOV"
m[11] = "DEC"

nm = length(m);

}

/^JAN|^FEB|^MAR|^APR|^MAY|^JUN|^JUL|^AUG|^SEP|^OCT|^NOV|^DEC/ {
  mon = $1
  getline; split($0,a); for (i in a) kwh[mon,i] = a[i]
  getline; split($0,a); for (i in a) maxkwh[mon,i] = a[i]
  getline;getline; split($0,a); for (i in a) peakenduse[mon,i] = a[i]
}

END {

  printf "Category, "; for (i=0; i<nm-1; i++) printf "%s, ", m[i]; printf "%s\n", m[11];
  for (i=0; i<nc; i++) { # One line per category
    printf "%s, ", c[i]
    for (j=0; j<nm-1; j++) # One column per month
      printf "%0.2f, ", kwh[m[j],i+2]
    printf "%0.2f\n", kwh[m[nm-1],i+2];
  }
  
}

' > preprocess/$(echo $name)_monthly_elec.csv

awk < 'reports/PS-E Energy End-Use Summary for all Fuel Meters' '
BEGIN {

# Energy categories

c[0] = "Area Lights"
c[1] = "Task Lights"
c[2] = "Misc Equip"
c[3] = "Space Heating"
c[4] = "Space Cooling"
c[5] = "Heat Reject"
c[6] = "Pumps & Aux"
c[7] = "Vent Fans"
c[8] = "Refrig Display"
c[9] = "Ht Pump Supplem"
c[10] = "Domest Hot Wtr"
c[11] = "Ext Usage"
c[12] = "Total"

nc = length(c)

# Month labels

m[0] = "JAN"
m[1] = "FEB"
m[2] = "MAR"
m[3] = "APR"
m[4] = "MAY"
m[5] = "JUN"
m[6] = "JUL"
m[7] = "AUG"
m[8] = "SEP"
m[9] = "OCT"
m[10] = "NOV"
m[11] = "DEC"

nm = length(m);

}

/^JAN|^FEB|^MAR|^APR|^MAY|^JUN|^JUL|^AUG|^SEP|^OCT|^NOV|^DEC/ {
  mon = $1
  getline; split($0,a); for (i in a) mbtu[mon,i] = a[i]
  getline; split($0,a); for (i in a) maxmbtuhr[mon,i] = a[i]
  getline;getline; split($0,a); for (i in a) peakenduse[mon,i] = a[i]
}

END {

  printf "Category, "; for (i=0; i<nm-1; i++) printf "%s, ", m[i]; printf "%s\n", m[11];
  for (i=0; i<nc; i++) { # One line per category
    printf "%s, ", c[i]
    for (j=0; j<nm-1; j++) # One column per month
      printf "%0.2f, ", mbtu[m[j],i+2]
    printf "%0.2f\n", mbtu[m[nm-1],i+2];
  }

}

' > preprocess/$(echo $name)_monthly_gas.csv

cat <<EOF > preprocess/$(echo $name)_hourly.csv
,,,Day,"Var 1","Var 2","Var 3","Var 4","Var 5","Var 6","Var 7","Var 8","Var 9","Var 10","Var 11","Var 12","Var 20","Var 1","Var 2","Var 3","Var 4","Var 5","Var 6","Var 7","Var 8","Var 9","Var 10","Var 11","Var 12","Var 20",
Month,Day,Hour,Type,"Lighting end-use energy","Task lighting end-use energy","Miscellaneous equipment end-use energy","Heating end-use energy","Cooling end-use energy","Heat rejection end-use energy","Auxiliary end-use energy (pumps)","Vent fan end-use energy","Refrigeration systems end-use energy","Supplemental heat pump end-use energy","Domestic hot water end-use energy","Exterior to the building end-use energy","Total end-use energy","Lighting end-use energy","Task lighting end-use energy","Miscellaneous equipment end-use energy","Heating end-use energy","Cooling end-use energy","Heat rejection end-use energy","Auxiliary end-use energy (pumps)","Vent fan end-use energy","Refrigeration systems end-use energy","Supplemental heat pump end-use energy","Domestic hot water end-use energy","Exterior to the building end-use energy","Total end-use energy",
EOF

awk < 'reports/Hourly Report HVAC' '

# Report columns for EM and FM (Elec Meter and Fuel Meter) are emitted in 3 groups, as follows:

# EM v1-v20                  ( 1) ( 2) ( 3) ( 4) ( 5) ( 6) ( 7) ( 8) ( 9) (10)
# EM v11-v12, v20; FM v1-v8: (11) (12) (20) ( 1) ( 2) ( 3) ( 4) ( 5) ( 6) ( 7) ( 8)
# FM v9-v12, v20:            ( 9) (10) (11) (12) (20)

/^ *----\( 1\)/ { reportrange = "v1_10" }
/^ *----\(11\)/ { reportrange = "v11_8" }
/^ *----\( 9\)/ { reportrange = "v9_20" }

/^ *[1-9]/ {
  m = int(substr($0,1,2))
  d = int(substr($0,3,2))
  h = int(substr($0,5,2))
  gsub(/^....../,"",$0)   # Remove fixed width fields
  if ( reportrange == "v1_10" ) {
    for(v=1; v<=10; v++) {
      EM[m,d,h,v] = $v
    }
  }
  else if ( reportrange == "v11_8" ) {
    EM[m,d,h,11] = $1
    EM[m,d,h,12] = $2
    EM[m,d,h,20] = $3
    for(v=1; v<=8; v++) {
      FM[m,d,h,v] = $(v+3)
    }
  }
  else if ( reportrange == "v9_20" ) {
    FM[m,d,h,9]  = $1
    FM[m,d,h,10] = $2
    FM[m,d,h,11] = $3
    FM[m,d,h,12] = $4
    FM[m,d,h,20] = $5
  }
}

END {

  dtype = 42
  for(m=1; m<=12; m++) {
    for(d=1; d<=31; d++) {
      for(h=1; h<=24; h++) {
         _ = SUBSEP
        i = m _ d _ h _ "1"
        if( i in EM ) {
          printf("%d, %d, %d, %d,", m, d, h, dtype)
          for(v=1;v<=12;v++) printf("%.4f, ", EM[m,d,h,v])
          printf("%.4f, ", EM[m,d,h,20])
          for(v=1;v<=12;v++) printf("%.4f, ", FM[m,d,h,v])
          printf("%.4f\n", FM[m,d,h,20])
        }
      }
    }
  }

}
' >> preprocess/$(echo $name)_hourly.csv
 
}


# run the filtering functions
echo "Starting Preprocess Filtering"
(
  filterdoe2 $1 > /dev/null
  getvars -h run001 baseline
  getdetails baseline

  filterdoe2 $2 > /dev/null
  getvars   run002 proposed
  getdetails proposed
  
  rm -rf reports
  
) > preprocess/summary.csv
echo "Preprocess Filtering Commplete"

# run the python post processor
echo "Running Python Post-Processor"
python postprocess.py preprocess/summary.csv $3 $4 $5

echo "Post-Processing Complete"
